@extends('application.layouts.app')

@section('content')
    @foreach($items as $item)
        <iframe width="375" height="211"  src="{{$item->url}}" frameborder="0" allowfullscreen></iframe>
        <br>
    @endforeach
@endsection

