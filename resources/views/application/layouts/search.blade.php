<div class="field">
    <p class="control has-icons-left">
        <input class="input is-black is-medium" type="text" placeholder="search">
        <span class="icon is-medium is-left">
      <i class="fa fa-search"></i>
    </span>
    </p>
</div>