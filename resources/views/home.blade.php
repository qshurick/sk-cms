@extends('application.layouts.app')

@section('content')
    <!-- Slider main container -->
    <div class="swiper-container-v">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            @foreach($assets as $asset)
                <div class="swiper-slide">
                    <div class="container">
                        <h1 class="title">
                            {{ $asset->title }}
                        </h1>
                    </div>
                    <video id="video{{$loop->iteration}}"
                           width="320"
                           preload="auto"
                           controls="true"
                           poster="/sk-cms/storage/app/images/{{$asset->poster}}"
                           src="/sk-cms/storage/app/video/{{$asset->asset}}"></video>
                </div>
            @endforeach

        </div>
    </div>
@endsection


