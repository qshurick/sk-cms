<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="/sk-cms/public/css/bulma.css">
    <link rel="stylesheet" href="/sk-cms/public/css/admin.css">
</head>
<body>
@include('admin.layouts.nav')
<section class="hero is-primary">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                @yield('title')
            </h1>
@yield('call2action')
        </div>
    </div>
</section>
<section class="section">
<div class="container">
    @yield('content')
</div>
</section>
<script src="/sk-cms/public/js/jquery-3.2.1.min.js"></script>
<script src="/sk-cms/public/js/admin.js"></script>
</body>
</html>
