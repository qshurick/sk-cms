@extends('admin.layouts.app')

@section('title')
    Admin add {{ $category->name }}
@endsection


@section('content')
    @include('admin.layouts.errors')
    <form action="/sk-cms/public/admin/create" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="field">
            <label class="label">Title</label>
            <p class="control">
                <input class="input" type="text" name="title" placeholder="Text input">
            </p>
        </div>

        <div class="field">
            <label class="label">Category</label>
            <p class="control">
                <input type="text" name="category" class="input" value="{{ $category->name  }}" readonly>
                <input type="hidden" name="category_id" class="input" value="{{ $category->id  }}" readonly>
            </p>
        </div>

        <div class="field">
            <label class="label">Poster (preview)</label>
            <p class="control">
                <input type="file" name="poster" class="file" accept="image/*">
            </p>
            <p class="help is-success">Image placeholder</p>
        </div>

        <div class="field">
            <label class="label">Asset</label>
            <p class="control">
            @if($category->name === 'video')
                    <input type="file" name="asset" class="file" accept="video/*">
            @elseif($category->name === 'sound')
                    <input type="file" name="asset" class="file" accept="sound/*">
            @elseif($category->name === 'game')
                    <input type="file" name="asset" class="file" accept="application/zip,application/gzip">
            @else()
                    <input type="file" name="asset" class="file">
            @endif
            </p>
            <p class="help is-success">Video, Sounds or Wallpapers</p>
        </div>

        <div class="field">
            <label class="checkbox label">
                <input type="checkbox" name="promoted">
                Promoted
            </label>
        </div>


        <div class="field is-grouped">
            <p class="control">
                <button class="button is-primary">Submit</button>
                <a class="button is-danger" href="/sk-cms/public/admin/categories/{{ $category->name  }}">Back</a>
            </p>
        </div>
    </form>
@endsection