@extends('admin.layouts.app')

@section('title','Admin edit categorie')

@section('call2action')
    <a class="button is-info is-inverted is-outlined" href="/sk-cms/public/admin/categories/{{ $category->name  }}/assets">Add {{ $category->name }}</a>
@endsection

@section('content')
    @include('admin.layouts/errors')
    <form action="/sk-cms/public/admin/categories/{{$category->id}}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="field">
            <label class="label">Name</label>
            <p class="control">
                <input class="input" name="name" type="text" value="{{$category->name}}">
            </p>
        </div>
        <div class="field is-grouped">
            <p class="control">
                <button class="button is-primary">Submit</button>
                <a href="/sk-cms/public/admin/categories" class="button is-outlined">Cancel</a>
            </p>
        </div>
    </form>
    <ul style="display: flex; flex-wrap: wrap;">
        @foreach($assets as $asset)
            <li style="width: 20%; padding: 1vw;">
                <div class="card {{ $asset->promoted ? 'promoted':'' }}">
                    <div class="card-image">
                        <figure class="image">
                            <div style="width=200px;height:200px;background:url('/sk-cms/storage/app/images/{{ $asset->poster  }}');background-size:cover;background-position:center"></div>
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-content">
                                <p class="title is-5">{{ $asset->title  }}</p>
                                <p class="subtitle is-6">@admin</p>
                            </div>
                        </div>

                        <div class="content">
                            <time class="subtitle is-6" datetime="{{ $asset->created_at  }}">{{ $asset->created_at  }}</time>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
@endsection
