@extends('admin.layouts.app')

@section('title','Admin categories')

@section('call2action')
    <a class="button is-info is-inverted is-outlined" href="/sk-cms/public/admin/categories/create">Add categorie</a>
@endsection

@section('content')
    <table class="table">
        @foreach($categories as $category)
            <tr>
                <td>
                    <a href="/sk-cms/public/admin/categories/{{$category->name}}">{{$category->name}}</a>
                </td>
                <td>
                    <form action="/sk-cms/public/admin/categories/delete/{{$category->id}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" value="" class="button is-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection