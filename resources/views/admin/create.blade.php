@extends('admin.layouts.app')

@section('title','Admin add content')


@section('content')
    @include('admin.layouts.errors')
    <form action="/sk-cms/public/admin/create" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="field">
            <label class="label">Title</label>
            <p class="control">
                <input class="input" type="text" name="title" placeholder="Text input">
            </p>
        </div>

        <div class="field">
            <label class="label">Categorie</label>
            <p class="control">
    <span class="select">
        <select name="category_id">
            <option selected="true" disabled="disabled" >Select dropdown</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
    </span>
            </p>
        </div>

        <div class="field">
            <label class="label">Poster</label>
            <p class="control">
                <input type="file" name="poster" class="file">
            </p>
            <p class="help is-success">Image placeholder</p>
        </div>

        <div class="field">
            <label class="label">Asset</label>
            <p class="control">
                <input type="file" name="asset" class="file">
            </p>
            <p class="help is-success">Video, Sounds or Wallpapers</p>
        </div>


        <div class="field is-grouped">
            <p class="control">
                <button class="button is-primary">Submit</button>
            </p>
        </div>
    </form>
@endsection