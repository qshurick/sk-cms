@extends('admin.layouts.app')

@section('title','Admin index')
@section('call2action')
    <a class="button is-info is-inverted is-outlined" href="/sk-cms/public/admin/create">Add content</a>
@endsection
@section('content')
    <table class="table">
        <thead>Head</thead>
        <tbody>
        @foreach($assets as $asset)
            <tr>
                <td>{{$asset->title}}</td>
                <td><img  width="50" src="/sk-cms/storage/app/images/{{$asset->poster}}"></td>
                <td>
                    <img width="50" src="/sk-cms/storage/app/video/{{$asset->asset}}">
                </td>

                <td>
                    <form action="/sk-cms/public/admin/delete/{{$asset->id}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" value="" class="button is-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection