$(document).ready(function () {
    $('#tags-input').keyup(function () {
        if($('#tags-input').val() == ''){
            $('#tags-suggestion').html('');
        }
        $.ajax({
            method: "GET",
            dataType: "json",
            url: "/shareking/public/admin/tags",
            data: {search: $('#tags-input').val()}
        }).done(function (results) {
            $('#tags-suggestion').html('');
            for (var i = 0; i < results.length; i++) {
                $('#tags-suggestion').append('<tr><td>' + results[i] + '</td></tr>');
            }
            $('#tags-suggestion td').on("click keyup", function (e) {
                console.log(e.keyCode);
                if (e.type == "click" || e.keyCode == 13) {
                    $('#tags').append(
                        '<span class="tag is-success">' +
                        $(this).html() +
                        '<button class="delete is-small"></button>' +
                        '</span>'
                    )
                }
            });
        }).fail(function () {
            console.log('fail');
        });
    });
});

