<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function videos()
    {
        $videos = Asset::where('category_id', 21)->get();
        //dd($assets[9]['original']['id']);
        return response()->json(["assets" => $videos]);
        //return view('home',compact('assets'));
    }

    public function sounds()
    {
        $sounds = Asset::where('category_id', 22)->get();
        //dd($assets[9]['original']['id']);
        return response()->json(["assets" => $sounds]);
        //return view('home',compact('assets'));
    }

    public function wallpapers()
    {
        $wallpapers = Asset::where('category_id', 29)->get();
        //dd($assets[9]['original']['id']);
        return response()->json(["assets" => $wallpapers]);
        //return view('home',compact('assets'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showCategoryItems($id)
    {
        $categoryItems = Asset::where('category_id', $id)->get();
        return view('application/category', compact('categoryItems'));
    }

    public function showCategoryAssets($categoryName)
    {
        $category = Category::where('name', $categoryName)->get()->first();
        $assets = $category->asset()->get();
        return response()->json(["assets" => $assets]);
    }

    public function showItems($id)
    {
        $items = Asset::where('category_id', $id)->get();
        return view('application/single', compact('items'));
    }


}
